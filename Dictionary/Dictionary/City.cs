﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    class City: Dictionary<string, Street>
    {
        public string CityName { get; set; }

        public City(string cityName)
        {
            this.CityName = cityName;
        }

        public void Add(Street street)
        {
            if (!ContainsKey(street.StreetName))
                Add(street.StreetName, street);
        }

        public override string ToString()
        {
            return "City: " + CityName;
        }
    }
}
