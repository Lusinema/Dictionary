﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    class Country : Dictionary<string, City>
    {
        public string CountryName { get; set; }

        public Country(string countryName)
        {
            this.CountryName = countryName;
        }

        public void Add(City city)
        {
            if (!ContainsKey(city.CityName))
                Add(city.CityName, city);
        }

        public override string ToString()
        {
            return "Country - " + CountryName;
        }
    }
}
