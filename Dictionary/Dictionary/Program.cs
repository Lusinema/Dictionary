﻿using System;
using System.Collections.Generic;


namespace Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            var c1 = new Country("Armenia");
            var c2 = new Country("Ireland");
            var c3 = new Country("USA");

            var cy1 = new City("Yerevan");
            var cy2 = new City("Vanadzor");
            var cy3 = new City("Gyumri");

            var st1 = new Street("Arshakunyac");
            var st2 = new Street("Meliqyan");

            var b1 = new Building("1a");
            var b2 = new Building("2a");
            var b3 = new Building("3a");
          
            st1.Add(b1);
            st1.Add(b2);
            st1.Add(b3);

            st2.Add(b1);
            st2.Add(b2);
            st2.Add(b3);

            cy1.Add(st1);
            cy1.Add(st2);
           
            c1.Add(cy1);
            c1.Add(cy2);
            c1.Add(cy3);

           
            foreach (var item in c1)
            {
                Console.WriteLine(item.Value);
                foreach (var item1 in item.Value)
                {
                    Console.WriteLine(item1.Value);
                    foreach (var item2 in item1.Value)
                    {
                        Console.WriteLine(item2.Value);
                    }
                }                
            }

            Console.ReadLine();

        }
    }
}
