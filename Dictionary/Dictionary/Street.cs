﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    class Street : Dictionary<string, Building>
    {
        public string StreetName { get; set; }

        public Street(string streetName)
        {
            this.StreetName = streetName;
        }

        public void Add(Building building)
        {
            if (!ContainsKey(building.BuildingNumber))
                Add(building.BuildingNumber, building);
        }
        public override string ToString()
        {
            return StreetName + " str. ";
        }
    }
}
